"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const querySchema_1 = require("./schema/querySchema");
const graphqlHTTP = require("express-graphql");
const express = require("express");
let app = express();
app.post('/', graphqlHTTP({
    schema: querySchema_1.default,
    graphiql: false,
    pretty: true
}));
app.get('/', graphqlHTTP({
    schema: querySchema_1.default,
    graphiql: true
}));
const PORT = process.env.PORT || 3000;
let server = app.listen(PORT, function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log(`Example app listening at http://${host}:${port}.`);
});
//# sourceMappingURL=app.js.map