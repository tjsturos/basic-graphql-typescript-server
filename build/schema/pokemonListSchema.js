"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const PokemonListType = new graphql_1.GraphQLObjectType({
    name: 'PokemonList',
    description: 'Details for the current pokemons list.',
    fields: () => ({
        pokemon: {
            type: new graphql_1.GraphQLList(exports.PokemonListItemType),
            resolve: list => list.results
        },
        nextPage: {
            type: graphql_1.GraphQLString,
            resolve: list => list.next
        },
        previousPage: {
            type: graphql_1.GraphQLString,
            resolve: list => list.previous
        }
    }),
});
exports.PokemonListItemType = new graphql_1.GraphQLObjectType({
    name: 'PokemonListItem',
    description: 'A reference to the pokemon\'s page.',
    fields: () => ({
        id: {
            type: graphql_1.GraphQLInt,
            resolve: pokemon => Number(pokemon.url.split("/")[6])
        },
        name: {
            type: graphql_1.GraphQLString,
            resolve: pokemon => pokemon.name
        },
        url: {
            type: graphql_1.GraphQLString,
            resolve: pokemon => pokemon.url
        }
    }),
});
exports.default = PokemonListType;
//# sourceMappingURL=pokemonListSchema.js.map