"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_1 = require("graphql/type");
const node_fetch_1 = require("node-fetch");
const pokemonSchema_1 = require("./pokemonSchema");
const pokemonListSchema_1 = require("./pokemonListSchema");
const BASE_URL = 'http://pokeapi.co/api/v2';
function fetchPageByUrl(wholeURL) {
    return node_fetch_1.default(wholeURL).then(res => res.json());
}
function fetchResponseViaUrl(relativeURL = "") {
    return node_fetch_1.default(`${BASE_URL}${relativeURL}`).then(res => res.json());
}
function fetchPokemon(page = 0) {
    return fetchResponseViaUrl(`/pokemon?offset=${page * 20}`).then(json => json);
}
function fetchPokemonByURL(relativeURL) {
    return fetchResponseViaUrl(relativeURL).then(json => json);
}
function fetchPokemonCount() {
    return fetchResponseViaUrl("/pokemon").then(json => json.count);
}
let schema = new type_1.GraphQLSchema({
    query: new type_1.GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            listPokemon: {
                type: pokemonListSchema_1.default,
                description: "Get a list of 20 pokemon.",
                args: {
                    page: {
                        type: type_1.GraphQLInt,
                        defaultValue: 0,
                        description: "The page number to use to navigate through all the pokemon."
                    }
                },
                resolve: (root, args) => fetchPokemon(args.page)
            },
            count: {
                description: "The number of pokemon that exist.",
                type: type_1.GraphQLInt,
                resolve: fetchPokemonCount
            },
            pokemon: {
                description: "The details for a specific pokemon.",
                type: pokemonSchema_1.default,
                args: {
                    id: {
                        type: type_1.GraphQLInt,
                        description: "The pokemon's id number. E.G. Bulbasaur is 1."
                    },
                },
                resolve: (root, args) => fetchPokemonByURL(`/pokemon/${args.id}`)
            }
        })
    })
});
exports.default = schema;
//# sourceMappingURL=querySchema.js.map