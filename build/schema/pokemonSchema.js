"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const node_fetch_1 = require("node-fetch");
const extendedPokemonInfo_1 = require("./extendedPokemonInfo");
function extendedDetailsViaURL(url) {
    return node_fetch_1.default(url).then(res => res.json());
}
const PokemonType = new graphql_1.GraphQLObjectType({
    name: 'Pokemon',
    description: 'A strange fictional animal with super powers.',
    fields: () => ({
        id: { type: graphql_1.GraphQLInt },
        name: {
            type: graphql_1.GraphQLString,
            resolve: pokemon => pokemon.species.name
        },
        url: {
            type: graphql_1.GraphQLString,
            resolve: pokemon => pokemon.species.url
        },
        picture: {
            type: graphql_1.GraphQLString,
            resolve: pokemon => pokemon.sprites['front_default']
        },
        types: {
            type: new graphql_1.GraphQLList(NatureType),
            resolve: pokemon => pokemon.types
        },
        additionalDetails: {
            type: extendedPokemonInfo_1.default,
            resolve: pokemon => extendedDetailsViaURL(pokemon.species.url)
        }
    }),
});
const NatureType = new graphql_1.GraphQLObjectType({
    name: "Nature",
    description: "The pokemon's nature type.",
    fields: () => ({
        type: {
            type: graphql_1.GraphQLString,
            resolve: overview => overview.type.name
        }
    }),
});
exports.default = PokemonType;
//# sourceMappingURL=pokemonSchema.js.map