"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pokemonListSchema_1 = require("./pokemonListSchema");
const graphql_1 = require("graphql");
const ExtendedPokemonInfoType = new graphql_1.GraphQLObjectType({
    name: 'PokemonDetails',
    description: 'More details about a pokemon.',
    fields: () => ({
        evolvesFrom: {
            type: pokemonListSchema_1.PokemonListItemType,
            description: "The previous pokemon in the evolution chain.",
            resolve: details => details['evolves_from_species']
        },
        habitat: {
            type: graphql_1.GraphQLString,
            description: "Where this pokemon likes to live.",
            resolve: details => details.habitat.name
        },
        shape: {
            type: graphql_1.GraphQLString,
            descrition: "What kind of shape this pokemon comes in.",
            resolve: details => details.shape.name
        },
        color: {
            type: graphql_1.GraphQLString,
            description: "The color of the pokemon.",
            resolve: details => details.color.name
        }
    }),
});
exports.default = ExtendedPokemonInfoType;
//# sourceMappingURL=extendedPokemonInfo.js.map