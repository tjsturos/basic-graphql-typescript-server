"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_1 = require("graphql/type");
let count = 0;
let schema = new type_1.GraphQLSchema({
    query: new type_1.GraphQLObjectType({
        name: 'RootQueryType',
        fields: {
            count: {
                type: type_1.GraphQLInt,
                description: 'The count!',
                resolve: function () {
                    return count;
                }
            }
        }
    })
});
exports.default = schema;
//# sourceMappingURL=schema.js.map