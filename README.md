# basic-graphql-typescript-server

## To Run Locally
1. Using Git
  - Run the command `git clone https://github.com/TygerTy/basic-graphql-typescript-server.git <optional-directory-name>`
2. Using ZIP File
  - Download ZIP
    - Click the button above that reads, "Clone or Download"
    - Choose the option to "Download ZIP"
    - After downloaded unzip to any directory
    - Open and navigate Terminal or Command Prompt to the directory location
3. Install packages
  - Depending on your package manager, run `yarn` or `npm install`.
4. Run `yarn start` or `npm start`
5. Navigate to `localhost:3001/graphql` or POST to `localhost:3001/graphql`

## Schema
### QueryType
```
{
  "data": {
    "__schema": {
      "queryType": {
        "fields": [
          {
            "name": "listPokemon",
            "description": "Get a list of 20 pokemon.",
            "args": [
              {
                "description": "The page number to use to navigate through all the pokemon.",
                "defaultValue": "0"
              }
            ]
          },
          {
            "name": "count",
            "description": "The number of pokemon that exist.",
            "args": []
          },
          {
            "name": "pokemon",
            "description": "The details for a specific pokemon.",
            "args": [
              {
                "description": "The pokemon's id number. E.G. Bulbasaur is 1.",
                "defaultValue": null
              }
            ]
          }
        ]
      }
    }
  }
}
```
