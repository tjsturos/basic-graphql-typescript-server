import {
    GraphQLList,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import fetch from 'node-fetch';
import ExtendedPokemonInfoType from './extendedPokemonInfo';
function extendedDetailsViaURL(url: string): object {
    return fetch(url).then(res => res.json());
}

const PokemonType: GraphQLObjectType = new GraphQLObjectType({
    name: 'Pokemon',
    description: 'A strange fictional animal with super powers.',
    fields: () => ({
        id: {type: GraphQLInt},
        name: {
            type: GraphQLString,
            resolve: pokemon => pokemon.species.name 
        },
        url: {
            type: GraphQLString,
            resolve: pokemon => pokemon.species.url
        },
        picture: {
            type: GraphQLString,
            resolve: pokemon => pokemon.sprites['front_default']
        },
        types: {
            type: new GraphQLList(NatureType),
            resolve: pokemon => pokemon.types
        },
        additionalDetails: {
            type: ExtendedPokemonInfoType,
            
            resolve: pokemon => extendedDetailsViaURL(pokemon.species.url)
        }
    }),
});

const NatureType: GraphQLObjectType = new GraphQLObjectType({
    name: "Nature",
    description: "The pokemon's nature type.",
    fields: () => ({
        type: {
            type: GraphQLString,
            resolve: overview => overview.type.name
        }
    }),
});

export default PokemonType;