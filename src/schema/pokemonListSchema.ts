import {
    GraphQLList,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} from 'graphql';

const PokemonListType: GraphQLObjectType = new GraphQLObjectType({
    name: 'PokemonList',
    description: 'Details for the current pokemons list.',
    fields: () => ({
        count: {
            type: GraphQLInt,
            resolve: list => list.count
        },
        pokemon: {
            type: new GraphQLList(PokemonListItemType),
            resolve: list => list.results
        },
        nextPage: {
            type: GraphQLString,
            resolve: list => list.next
        },
        previousPage: {
            type: GraphQLString,
            resolve: list => list.previous
        }
    }),
});

export const PokemonListItemType: GraphQLObjectType = new GraphQLObjectType({
    name: 'PokemonListItem',
    description: 'A reference to the pokemon\'s page.',
    fields: () => ({
        id: {
            type: GraphQLInt,
            resolve: pokemon => Number(pokemon.url.split("/")[6])
        },
        name: {
            type: GraphQLString,
            resolve: pokemon => pokemon.name
        },
        url: {
            type: GraphQLString,
            resolve: pokemon => pokemon.url

        }
    }),
});


export default PokemonListType;