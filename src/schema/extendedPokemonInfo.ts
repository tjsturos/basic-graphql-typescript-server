import { PokemonListItemType } from './pokemonListSchema';
import {
    GraphQLList,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} from 'graphql';

const ExtendedPokemonInfoType: GraphQLObjectType = new GraphQLObjectType({
    name: 'PokemonDetails',
    description: 'More details about a pokemon.',
    fields: () => ({
       evolvesFrom: {
           type: PokemonListItemType!,
           description: "The previous pokemon in the evolution chain.",
           resolve: details => details['evolves_from_species']
       },
       habitat: {
           type: GraphQLString,
           description: "Where this pokemon likes to live.",
           resolve: details => details.habitat.name
       },
       shape: {
           type:GraphQLString,
           descrition: "What kind of shape this pokemon comes in.",
           resolve: details => details.shape.name
       },
       color: {
           type: GraphQLString,
           description: "The color of the pokemon.",
           resolve: details => details.color.name
       }
    }),
});

export default ExtendedPokemonInfoType;