// src/schema.ts
import {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLInt,
    GraphQLList,
    GraphQLString
} from 'graphql/type';

import fetch from 'node-fetch';
import PokemonType from './pokemonSchema';
import PokemonListType from './pokemonListSchema';

const BASE_URL: string = 'http://pokeapi.co/api/v2';

function fetchPageByUrl(wholeURL: string) {
    return fetch(wholeURL).then(res => res.json());
}
function fetchResponseViaUrl(relativeURL: string = "") {
    return fetch(`${BASE_URL}${relativeURL}`).then(res => res.json());
}

function fetchPokemon(page: number = 0) {
    // there are quite a few pokemon, so there is only 20 on a page => we request the set of 20 based on the page number
    return fetchResponseViaUrl(`/pokemon?offset=${page*20}`).then(json => json);
}

function fetchPokemonByURL(relativeURL: string) {
    return fetchResponseViaUrl(relativeURL).then(json => json);
}

function fetchPokemonCount() {
    return fetchResponseViaUrl("/pokemon").then(json => json.count);
}

let schema: GraphQLSchema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: () => ({
            listPokemon: {
                type: PokemonListType,
                description: "Get a list of 20 pokemon.",
                args: {
                    page: {
                        type: GraphQLInt, 
                        defaultValue: 0, 
                        description: "The page number to use to navigate through all the pokemon."
                    }   
                },
                resolve: (root, args) => fetchPokemon(args.page)
            },
            pokemon: {
                description: "The details for a specific pokemon.",
                type: PokemonType,
                args: {
                    id: { 
                        type: GraphQLInt, 
                        description: "The pokemon's id number. E.G. Bulbasaur is 1."
                    },
                   
                },
                resolve: (root, args) => fetchPokemonByURL(`/pokemon/${args.id}`)
            }
        })    
    })
});

export default schema;