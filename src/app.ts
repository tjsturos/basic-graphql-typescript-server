// src/app.ts
import schema from './schema/querySchema';

import graphqlHTTP = require('express-graphql');
import express = require('express');

let app = express();


app.post('/', graphqlHTTP({
    schema,
    graphiql: false,
    pretty: true
}));

app.get('/', graphqlHTTP({
    schema,
    graphiql: true
}));

const PORT = process.env.PORT || 3000;
let server = app.listen(PORT, function() {
    let host: string = server.address().address;
    let port: number = server.address().port;
  console.log(`Example app listening at http://${host}:${port}.`);
});